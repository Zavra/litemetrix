$(document).ready(function () {

    $('.details__show-chart').on('click', function() {
        var chartId = $(this).data('chart');
        $('#' + chartId).slideToggle();
        $(this).toggleClass('collapsed');
    });

    if ($(".owl-carousel").length) {
        $(".owl-carousel").owlCarousel({
            loop:true,
            nav:true,
            items: 1,
            animateOut: 'fadeOut'
        });
    }

});


jQuery(document).ready(function() {
    AppMain.init();
});

var AppMain = function () {
    var initTopPersonShow = function () {
        var closetimer = 0;

        $("#nav-person-c").mouseenter(function () {
            services_show_timer();
        });

        //for mobile devices
        $("#nav-person-c").click(function () {
            if ($("#nav-persons-list").is(':visible')) {
                serevices_canceltimer();
                closetimer = window.setTimeout(function () {
                    $("#nav-persons-list").slideUp();
                    $('#nav-persons-list-caret').hide();
                }, 200);
            } else {
                services_show_timer();
            }
        });

        $("#nav-person-c, #nav-persons-list").mouseleave(function () {
            serevices_canceltimer();
            serevices_hide_timer();
        });

        function serevices_canceltimer() {
            if (closetimer) {
                window.clearTimeout(closetimer);
                closetimer = null;
            }
        }

        function serevices_hide_timer() {
            closetimer = window.setTimeout(function () {
                if ($('#nav-persons-list:hover').length == 0 && $('#nav-person-c:hover').length == 0) {
                    $("#nav-persons-list").slideUp();
                    $('#nav-persons-list-caret').hide();
                }
            }, 200);
        }

        function services_show_timer() {
            closetimer = window.setTimeout(function () {
                $("#nav-persons-list").slideDown();
                $('#nav-persons-list-caret').show();
            }, 200);
        }

    };

    var initRightMenuMobileShow = function() {

        $('.header__menu-icon').on('click', function() {
            $('.main-c').css('overflow', 'inherit'); //fix  - show menu from the very top
            $('.body__overlay').show();
            $('.left_categories-c').animate({left: "0px"}, 500, function() {
                $('.username-c').show();
            });

        });

        $('*[data-action="categories-close"]').on('click', function() {
            $('.main-c').css('overflow', 'hidden'); //fix  - show menu from the very top
            $('.username-c').hide();
            $('.left_categories-c').animate({left: "-300px"}, 500, function() {
                $('.body__overlay').hide();
            });

        })

    };

    var initLeftBlockShow = function() {
        //we use this variable to track if the dark-blue is already shown.
        // If block is already shown - just show another one without animation
        var detailsShown = 0;
        //show item details
        $('*[data-details]').on('click', function() {

            var detailsId = $(this).data('details');

            //$('.item-details-c').css({right: "-407px"});
            $('.item-details-c').removeClass('opened');
            $('.item-details-c').find('.details__add-new').css('display', 'none');

            if (detailsShown>0) {
                //$('#' + detailsId).css({right: "0px"});
                $('#' + detailsId).addClass('opened');
                $('#' + detailsId).find('.details__add-new').css('display', 'flex');

            } else {
                /*$('#' + detailsId).animate({right: "0px"}, 500, function() {
                    $('#' + detailsId).find('.details__add-new').css('display', 'flex');
                });*/

                $('#' + detailsId).switchClass( "", "opened", 500, "easeInOutQuad");
            }

            detailsShown++;

        });

        //close details block on the right
        $('*[data-action="details-close"]').on('click', function() {
            //$(this).closest('.item-details-c').animate({right: "-407px"}, 500);

            $(this).closest('.item-details-c').switchClass( "opened", "", 500, "easeInOutQuad");

            $(this).closest('.item-details-c').find('.details__add-new').css('display', 'none');
            detailsShown = 0;
        });
    };

    return {
        init: function () {
            initTopPersonShow(); //open list of persons in header on hover
            initRightMenuMobileShow(); //open menu on the left for mobile devices if click on burger icon
            initLeftBlockShow(); //open dark blue block on the right
        }
    };

}();