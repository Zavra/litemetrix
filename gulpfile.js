var gulp = require('gulp'); // Сообственно Gulp JS
var minifyCSS = require('gulp-minify-css'); // Минификация CSS
//var imagemin = require('gulp-imagemin'); // Минификация изображений
var uglify = require('gulp-uglify'); // Минификация JS
var concat = require('gulp-concat'); // Склейка файлов
var connect = require('connect'); // Webserver
var rename = require('gulp-rename'); //переименовывалка
var del = require('del'); //удалялка
var browserSync = require("browser-sync").create(); //синхронизирует изменения с браузером
var reload = browserSync.reload;
var svgSprite = require('gulp-svg-sprite') //svg спрайты
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');

var path = {
    build: {
        js: 'assets/js/',
        css: 'assets/css/',
        img: 'assets/i/',
        fonts: 'assets/fonts/'
    },
    watch: {
        js: 'resources/js/*.js',
        img: 'resources/i/**/*.*',
        fonts: 'resources/fonts/**/*.*',
        scss: 'resources/scss/**/*.scss'
        //svg: 'resources/i/svg/*.svg',
    }
};

// - SCSS LIBRARY
gulp.task('scss_library', function () {
    //сайт
    gulp.src([
        //"./vendor/bower_components/bootstrap4/scss/bootstrap-flex.scss", //bootstrap
        "./vendor/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css", //выпадающая бутстраповская дата
        //"./vendor/bower_components/font-awesome/scss/font-awesome.scss", //иконки http://fortawesome.github.io/Font-Awesome/
        "./vendor/bower_components/simple-line-icons/scss/simple-line-icons.scss", //иконки http://simplelineicons.com/
        "./vendor/bower_components/animate.css/animate.min.css", // https://owlcarousel2.github.io
        "./vendor/bower_components/owl.carousel/dist/assets/owl.carousel.min.css", // https://owlcarousel2.github.io
        "./vendor/bower_components/owl.carousel/dist/assets/owl.theme.default.min.css"
    ])
        .pipe(sourcemaps.init())
        .pipe(
            sass({
                includePaths: [],
                imagePath: path.watch.img
            })
            .on('error', sass.logError)
        )

        // https://github.com/ai/browserslist
        .pipe(autoprefixer("last 2 version", "> 1%", "Explorer >= 8", {
            cascade: true
        }))

        .pipe(concat('all_library.css'))
        .pipe(gulp.dest(path.build.css))
        .pipe(minifyCSS({compatibility: 'ie8'}))
        .pipe(rename({suffix: ".min"}))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(path.build.css))
        //.pipe(reload({stream: true}));
});

// - SCSS
gulp.task('scss', function () {
    //сайт
    gulp.src([
        path.watch.scss
    ])
        .pipe(sourcemaps.init())
        .pipe(
            sass({
                includePaths: [],
                imagePath: path.watch.img
            })
            .on('error', sass.logError)
        )

        // https://github.com/ai/browserslist
        .pipe(autoprefixer("last 2 version", "> 1%", "Explorer >= 8", {
            cascade: true
        }))

        .pipe(concat('all.css'))
        .pipe(gulp.dest(path.build.css))
        .pipe(minifyCSS({compatibility: 'ie8'}))
        .pipe(rename({suffix: ".min"}))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(path.build.css))
        //.pipe(reload({stream: true}));

});

// - JS LIBRARY
gulp.task('js_library', function() {
    //сайт
    gulp.src([
        "./vendor/bower_components/jquery/dist/jquery.js",
        "./vendor/bower_components/jquery-ui/jquery-ui.min.js",
        "./vendor/bower_components/bootstrap/js/dist/modal.js", //модальные окна
        "./vendor/bower_components/bootstrap/js/dist/util.js", //bootstrap
        "./vendor/bower_components/bootstrap/js/dist/collapse.js", //bootstrap
        "./vendor/bower_components/bootstrap/js/dist/tab.js", //bootstrap
        "./vendor/bower_components/moment/min/moment.min.js", //работа с датой через js
        "./vendor/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js", //выпадающая бутстраповская дата
        //"./vendor/bower_components/moment/locale/ru.js", //работа с датой через js
        "./vendor/bower_components/jquery-validation/dist/jquery.validate.js", //валидатор форм
        "./vendor/bower_components/owl.carousel/dist/owl.carousel.min.js",
    ])
    .pipe(concat('all_library.js'))
    .pipe(gulp.dest(path.build.js))
    .pipe(rename({suffix: ".min"}))
    .pipe(uglify()) //раскоменить после разработки - постоянно валится из за ошибок в js
    .pipe(gulp.dest(path.build.js))
    .pipe(reload({stream: true}));

});

// - JS
gulp.task('js', function() {
    //сайт
    gulp.src([
        path.watch.js
    ])
    .pipe(concat('all.js'))
    .pipe(gulp.dest(path.build.js))
    .pipe(rename({suffix: ".min"}))
    .pipe(uglify()) //раскоменить после разработки - постоянно валится из за ошибок в js
    .pipe(gulp.dest(path.build.js))
    .pipe(reload({stream: true}));

});

//создать виртуальный вебсервер
gulp.task('webserver', function () {
    browserSync.init({
        proxy: ""
    });
});

// Сжимаем картинки
gulp.task('images', function() {
    gulp.src(path.watch.img)
        //.pipe(imagemin({optimizationLevel: 5}))
        .pipe(gulp.dest(path.build.img))
        .pipe(reload({stream: true}));
});

//копируем шрифты
gulp.task('fonts', function() {
    gulp.src(path.watch.fonts)
    .pipe(gulp.dest(path.build.fonts))
});

//задания слежения
gulp.task('watch', function(event) {
    gulp.watch(path.watch.js, ['js']);
    gulp.watch(path.watch.img, ['images']);
    gulp.watch(path.watch.fonts, ['fonts']);
    gulp.watch(path.watch.scss, ['scss']);
});

//что б запустить только это вбиваем в терминал gulp build
gulp.task('build', [
    'js_library',
    'js',
    'scss_library',
    'scss',
    'fonts',
    'images'
]);

//что б запустить всё всё вбиваем в терминал gulp
gulp.task('default', [
    'build',
    //'webserver',
    'watch'
]);
